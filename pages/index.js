import React, { Component } from 'react'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import {Button} from 'reactstrap'
import Navbar from './components/NavbarComponent'

export default class index extends Component {

  state = {
    count: 1,
    image: 'https://jokesbapak2.herokuapp.com/v1/id/1'    
  }

  handleNextButton = async () => {    
    await this.setState({count: this.state.count + 1})
    await this.setState({image: `https://jokesbapak2.herokuapp.com/v1/id/${this.state.count}`})    

    console.log(this.state.count);
    console.log(this.state.image);
  }

  handlePreviousButton = async () => {
    if(this.state.count > 1){
      await this.setState({count: this.state.count - 1})
      this.setState({image: `https://jokesbapak2.herokuapp.com/v1/id/${this.state.count}`})
    }    

    console.log(this.state.count);
    console.log(this.state.image);
  }

  render() {  
    return (
      <div>
        <Head>
          <title>Jokes Bapacks</title>
          <meta name="description" content="Hanya iseng" />
          <link rel="icon" href="/favicon.ico" />
        </Head>             

        <Navbar/>

        <div className={styles.container}>
          <div className={styles.content}>          
            <img src={this.state.image} alt="jokes"/>          
          </div>

          <div className={styles.button}>
            <Button onClick={this.handlePreviousButton}>Sebelumnya</Button>
            <Button onClick={this.handleNextButton}>Selanjutnya</Button>
          </div>
        </div>

        <footer className={styles.footer}>
          <p>API from</p>
          <a href="https://jokesbapak2.pages.dev/api">https://jokesbapak2.pages.dev/api</a>
        </footer>
      </div>
    )
  }
}
