import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import styles from '../../styles/NavbarComponent.module.css'

const Example = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className={styles.navbar}>
      <Navbar dark expand="md">
        <NavbarBrand  className={styles.navbarBrand} href="/" clas>Jokes Bapacks</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="https://jokesbapak2.pages.dev/api" target="_blank">API Docs</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://gitlab.com/MRaehanR/jokes-bapacks" target="_blank">Gitlab</NavLink>
            </NavItem>        
          </Nav>          
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Example;